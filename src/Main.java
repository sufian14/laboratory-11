//program that allows storing undirected graph in adjacency list
import java.util.*;

public class Main {

	static class Graph {
		int vertices;
		LinkedList<Integer> adjListArray[];
		// constructor
		Graph(int V) {
			this.vertices = V;
			adjListArray = new LinkedList[V]; // define the size of array as number of vertices
			for (int i = 0; i < V; i++) { // Create a new list for each vertex to store adjacent nodes
				adjListArray[i] = new LinkedList<>();
			}
		}
	}

	// method creating an edge 
	static void makeEdge(Graph graph, int from, int to) {
		graph.adjListArray[from].add(to);
		graph.adjListArray[to].add(from);
	}

	// method printing the adjacency list representing the graph
	static void printGraph(Graph graph) {
		for (int v = 0; v < graph.vertices; v++) {
			System.out.print("Adjacency list of vertex " + v + ":");
			for (Integer pCrawl : graph.adjListArray[v]) {
				System.out.print(" -> " + pCrawl);
			}
			System.out.println("\n");
		}
	}

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of vertices: ");
		int V = input.nextInt();

		Graph graph = new Graph(V);
		int count = 0; // count number of edges

		System.out.println("Enter the edges: <from> <to> (type f to finish input)");
		while (true) {
			try {
				int from = input.nextInt();
				int to = input.nextInt();
				makeEdge(graph, from, to);
				count++;
			} catch (InputMismatchException e) {
				input.close();
				System.out.println("Number of edges: " + count);
				printGraph(graph);
				System.exit(0);
			}
		}
	}

}
